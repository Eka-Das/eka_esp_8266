void lecturas_clima() {
  h = dht.readHumidity();
  t = dht.readTemperature();

  if (isnan(h) || isnan(t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }
  temperatura = String(t);
  humedad = String(h);
  
//    Serial.print(F("Humidity: "));
    Serial.print(h);
//    Serial.print("%");

//    Serial.print(F("\nTemperature: "));
    Serial.print(t);
//    Serial.print(F("°C \n"));
  
}
