<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <title>Node</title>
  </head>
  <body>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    <center>

	    <h1>Interfaz</h1>
	    	<hr>
    	<div class="container">
		  <div class="row">
		    <div class="col-sm">
		      <p>Lecturas de temperatura</p>
		      <br>
		      <p id="lecturas"></p>
		      
		    </div>
		    <div class="col-sm">
		    	<hr>
		      <p >Botones</p>
		      <div class="form-check form-switch">
				  
				  
					<center>
						<input class="form-check-input position-absolute" name="switch_led" type="checkbox" id="switch_led">
						<label class="form-check-label" for="flexSwitchCheckDefault">Enciende un led</label>
				  </center>
				</div>
		    </div>
		    <div class="col-sm">
		    	<hr>
		      Envía un texto
		      <input class="form-control" type="text" placeholder="Imprimir algo en pantalla" aria-label="readonly input example" readonly>
		    </div>
		  </div>
		</div>		
    </center>
    



    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script type="text/javascript">
  	
    	$("#envio").click(function(){


			  $.get("consultas.php", {dato : "hola"}, function(data, status){

			  alert("Data: " + data + "\nStatus: " + status);
			});
		});
	    
    </script>

    <script type="text/javascript">
    	$(function (){
    		update();
    		
    		setInterval(update, 5000);
    	});

    	function update(){
    			$("#lecturas").load("consultas.php" , {temperatura : true});
    		}
    </script>

    <!----------Switch_Led ---------->
    <script type="text/javascript">
    	//consultar estado del boton
    	$(function(){
    		//alert("hola");
	    	$.get("consultas.php", {switch_led : true},function(data1){
	    		
	    		Number(data1);
	    		console.log(data1);
	    		
	    		if(data1 == 1){
	    		
	    			$("#switch_led").attr("checked", "true");

	    		}else if(data1 == 0){

	    		}
	    	});
	    });

    	$("#switch_led").change(function(){
    		
    		$(this).is(":checked") ? estado_switch = 0 : estado_switch = 1;
			
			$.get("consultas.php", {switch_led_change : estado_switch}, function(data1){
				console.log("recibiendo: " + data1);
			});
			
    		estado = $(this).val();
    	});
    </script>
  </body>
</html>