String contenido = "hola";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("Listo");
}

void loop() {
  // put your main code here, to run repeatedly:
  int valor = contenido.indexOf("hola");
  
  Serial.println("Valor: ");
  Serial.println(valor);

  if(valor == 0){
    Serial.println("Encender");
  }
  delay(3000);
}
