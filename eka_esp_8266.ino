#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

LiquidCrystal_I2C lcd(0x27,16,2);

#include "DHT.h"
#define DHTPIN 0
#define DHTTYPE DHT22
DHT dht(DHTPIN, DHTTYPE);

float h = 0;
float t = 0;
String temperatura;
String  humedad;
const char* ssid     = "Totalplay-5AA3";
const char* password = "5AA33498Pp9eJH6m";
const char* host = "www.matrixparatodos.esy.es";
String url = "/content/node/recepcion.php?";
int mensaje1 = 0;


HTTPClient http;

void setup()
{

  lcd.init();
  // Print a message to the LCD.
  lcd.backlight();
  lcd.setCursor(3,0);
  lcd.print("Hola mundo");

  Serial.begin(115200);
  delay(10);
  //Establecer la conexion con el modem
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.print("Conectando a:\t");
  Serial.println(ssid);

  // Esperar a que nos conectemos
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print('.');
  }

  // Mostrar mensaje de exito y dirección IP asignada
  Serial.println();
  Serial.print("Conectado a:\t");
  Serial.println(WiFi.SSID());
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());

  dht.begin();
}
void loop()
{
  WiFiClient client;

  Serial.printf("\n[Connecting to %s ... ", host);
  if (client.connect(host, 80))
  {
    Serial.println("connected]");

    lecturas_clima();

    Serial.println("[Sending a request]");
    Serial.println(url + "temp=" + temperatura + "&hum="+ humedad);
    client.print(String("GET ") + url + "temp=" + temperatura + "&hum="+ humedad + " HTTP/1.1\r\n" +
                 "Host: " + host +  "\r\n" +
                 "Connection: close\r\n\r\n");
    delay(500);
    Serial.println("[Response:]");

    while (client.connected() || client.available())
    {
      if (client.available())
      {
        String line = client.readStringUntil('\n');
        //String json = client.readStringUntil('3000');
        /*
          if (json) {
          Serial.println("CORECTO___");
          }
          
        */
        Serial.println(line);
      }
    }
    client.stop();
    Serial.println("\n[Disconnected]");
  }
  else
  {
    Serial.println("connection failed!]");
    client.stop();
  }
  delay(120000);
}
